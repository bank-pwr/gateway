<?php

namespace App\Exceptions;

use Exception;

class AuthKeyNotFoundException extends Exception
{
    protected $code = 401;
    protected $message = 'Unauthorized';
}
