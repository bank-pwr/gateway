<?php

namespace App\Exceptions;

use Exception;

class NoSuchProviderException extends Exception
{
    protected $code = 404;
    protected $message = 'No such provider!';
}
