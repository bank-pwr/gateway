<?php

namespace App\Exceptions;

use Monolog\Logger;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * @param Exception $exception
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }

    public static function log(Exception $exception) {
        $logger = new Logger('exceptions');
        $formatter = new LineFormatter(null, null, true, true);
        $handler= new StreamHandler(base_path().'/storage/app/logs/exceptions.log');
        $handler->setFormatter($formatter);
        $logger->pushHandler($handler);

        $text = "\nException: ".get_class($exception);
        $text .= "\nMessage: ".$exception->getMessage();
        $text .= "\n";

        $logger->error($text);
    }
}
