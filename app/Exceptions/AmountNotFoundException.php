<?php

namespace App\Exceptions;

use Exception;

class AmountNotFoundException extends Exception
{
    protected $code = 404;
    protected $message = 'Amount not found!';
}
