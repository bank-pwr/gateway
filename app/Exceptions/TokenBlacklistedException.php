<?php

namespace App\Exceptions;

use Exception;

class TokenBlacklistedException extends Exception
{
    protected $code = 401;
    protected $message = 'AUTH.REFRESH.TOKEN_BLACKLISTED';
}
