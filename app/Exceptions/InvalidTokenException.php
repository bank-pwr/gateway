<?php

namespace App\Exceptions;

use Exception;

class InvalidTokenException extends Exception
{
    protected $code = 401;
    protected $message = 'AUTH.REFRESH.INVALID_TOKEN';
}
