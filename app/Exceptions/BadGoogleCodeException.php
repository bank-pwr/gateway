<?php

namespace App\Exceptions;

use Exception;

class BadGoogleCodeException extends Exception
{
    protected $code = 401;
    protected $message = 'AUTH.BAD_CODE';
}
