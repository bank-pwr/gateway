<?php

namespace App\Exceptions;

use Exception;

class BadResponseException extends Exception
{
    protected $code = 419;
}
