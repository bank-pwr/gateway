<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Mail\Mailable;

class TransactionAcceptedMail extends Mailable
{
    protected User $user;
    protected float $transactionValue;

    /**
     * TransactionAcceptedMail constructor.
     * @param User $user
     * @param float $transactionValue
     */
    public function __construct(User $user, float $transactionValue)
    {
        $this->user = $user;
        $this->transactionValue = $transactionValue;
        $this->subject = 'Wpłata środków na konto';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.transaction')
            ->with([
                'user' => $this->user,
                'transaction_value' => $this->transactionValue
            ]);
    }
}
