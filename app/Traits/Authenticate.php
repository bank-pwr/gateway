<?php

namespace App\Traits;

use App\Authentication\Facades\Token;
use App\Exceptions\AuthKeyNotFoundException;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;

trait Authenticate
{
    /**
     * @param string $token
     * @return Response
     */
    protected function respondWithToken(string $token): Response {
        return response([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => 3600,
        ]);
    }
}
