<?php

namespace App\Http\Middleware\Services;

use Closure;
use Illuminate\Http\Request;

class ServiceKey
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $moduleName = $request->header('Module-name');
        $moduleKey = $request->header('Module-key');
        $key  = config("modules.{$moduleName}.gateway_key", null);

        if (is_null($key) || $key !== $moduleKey) {
            return response(['message' => 'Forbidden!'], 403);
        }
        return $next($request);
    }
}
