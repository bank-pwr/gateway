<?php

namespace App\Http\Requests;

use App\Contracts\Request;

class WalletRequest implements Request
{

    public function rules(): array
    {
        return [
            'wallet_id' => 'required|numeric',
        ];
    }

    public function messages(): array
    {
        return [

        ];
    }
}
