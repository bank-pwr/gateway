<?php

namespace App\Http\Requests;

use App\Contracts\Request;

class SendPasswordChangeEmailRequest implements Request
{

    public function rules(): array
    {
        return [
            'email' => 'required|email',
        ];
    }

    public function messages(): array
    {
        return [

        ];
    }
}
