<?php

namespace App\Http\Requests;

use App\Contracts\Request;

class LoginRequest implements Request
{

    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'email'],
            'password' => ['required', 'string'],
        ];
    }

    public function messages(): array
    {
        return [

        ];
    }
}
