<?php

namespace App\Http\Requests;

use App\Contracts\Request;

class CodeRequest implements Request
{

    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'email'],
            'code' => ['required', 'string'],
        ];
    }

    public function messages(): array
    {
        return [

        ];
    }
}
