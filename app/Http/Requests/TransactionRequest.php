<?php

namespace App\Http\Requests;

use App\Contracts\Request;

class TransactionRequest implements Request
{

    public function rules(): array
    {
        return [
            'transaction_id' => 'required|numeric',
        ];
    }

    public function messages(): array
    {
        return [

        ];
    }
}
