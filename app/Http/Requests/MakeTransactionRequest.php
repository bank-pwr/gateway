<?php

namespace App\Http\Requests;

use App\Contracts\Request;

class MakeTransactionRequest implements Request
{

    public function rules(): array
    {
        return [
            'to_wallet_number' => 'required|numeric',
            'amount' => 'required|numeric',
        ];
    }

    public function messages(): array
    {
        return [

        ];
    }
}
