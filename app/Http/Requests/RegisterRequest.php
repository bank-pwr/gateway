<?php

namespace App\Http\Requests;

use App\Contracts\Request;

class RegisterRequest implements Request
{

    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required', 'string', 'min:9', 'max:9'],
        ];
    }

    public function messages(): array
    {
        return [

        ];
    }
}
