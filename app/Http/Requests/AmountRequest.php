<?php

namespace App\Http\Requests;

use App\Contracts\Request;

class AmountRequest implements Request
{

    public function rules(): array
    {
        return [
            'amount' => 'required|numeric',
        ];
    }

    public function messages(): array
    {
        return [

        ];
    }
}
