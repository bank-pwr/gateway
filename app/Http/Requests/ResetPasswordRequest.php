<?php

namespace App\Http\Requests;

use App\Contracts\Request;

class ResetPasswordRequest implements Request
{

    public function rules(): array
    {
        return [
            'token' => 'required|string',
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }

    public function messages(): array
    {
        return [

        ];
    }
}
