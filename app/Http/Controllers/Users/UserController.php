<?php

namespace App\Http\Controllers\Users;

use App\Exceptions\Handler;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\TransactionsService;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Sonata\GoogleAuthenticator\GoogleQrUrl;

class UserController extends Controller
{
    protected TransactionsService $service;

    public function __construct(TransactionsService $service) {
        $this->service = $service;
    }

    public function user(Request $request) {
        try {
            $user = Auth::user();

            $walletResponse = $this->service->getUserInfo($user->{User::ID});

            return $this->response(true, 200, [
                'user' => $user->toArray(),
                'roles' => $user->getRoleNames()->toArray(),
                'permissions' => $user->getAllPermissions()->pluck('name')->toArray(),
                'wallet' => $walletResponse->wallet,
                'code' => GoogleQrUrl::generate($user->{User::EMAIL}, $user->getQrCode())
            ]);
        } catch (\Exception $exception) {
            Handler::log($exception);
            return $this->response(false, 401, ['message' => 'AUTH.FAIL']);
        }
    }
}
