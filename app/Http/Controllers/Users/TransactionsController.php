<?php

namespace App\Http\Controllers\Users;

use App\Events\TransactionAcceptedEvent;
use App\Exceptions\Handler;
use App\Http\Controllers\Controller;
use App\Http\Requests\MakeTransactionRequest;
use App\Http\Requests\TransactionRequest;
use App\Http\Requests\WalletRequest;
use App\Models\User;
use App\RequestValidators\RequestValidator;
use App\Services\TransactionsService;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionsController extends Controller
{
   protected TransactionsService $service;

   public function __construct(TransactionsService $service) {
       $this->service = $service;
   }

   public function makeTransaction(Request $request) {
       try {
           RequestValidator::validate($request, new MakeTransactionRequest());
           $user = Auth::user();
           $walletResponse = $this->service->getUserInfo($user->{User::ID});
           $walletNumber = $walletResponse->wallet->wallet_number;

           $this->service->makeTransaction($walletNumber, $request->to_wallet_number, $request->amount);

           return $this->response(true, 200, [

           ]);
       } catch (\Exception $exception) {
           Handler::log($exception);
           return $this->response(false, 419, ['message' => $exception->getMessage()]);
       }
   }

    public function getUserTransactions(Request $request) {
        try {
            $user = Auth::user();
            $response = $this->service->getUserTransactions($user->{User::ID});

            return $this->response(true, 200, [
                'inbound_transactions' => $response->inbound_transactions ?? [],
                'outgoing_transactions' => $response->outgoing_transactions ?? [],
            ]);
        } catch (\Exception $exception) {
            Handler::log($exception);
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }

    public function getAllTransactions() {
        try {
            $response = $this->service->getAllTransactions();

            return $this->response(true, 200, [
                'transactions' => $response->transactions ?? []
            ]);
        } catch (\Exception $exception) {
            Handler::log($exception);
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }

    public function acceptTransaction(Request $request) {
        try {
            RequestValidator::validate($request, new TransactionRequest());
            $response = $this->service->acceptTransaction($request->transaction_id);

            if ($response->success) {
                $user = User::find($response->transaction->accepted_wallet->user_id);
                $value = $response->transaction->value;
                event(new TransactionAcceptedEvent($user, $value));
            }

            return $this->response(true, 200);
        } catch (\Exception $exception) {
            Handler::log($exception);
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }

    public function rollbackTransaction(Request $request) {
        try {
            RequestValidator::validate($request, new TransactionRequest());
            $this->service->rollbackTransaction($request->transaction_id);

            return $this->response(true, 200);
        } catch (\Exception $exception) {
            Handler::log($exception);
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }

    public function getWallets() {
        try {
            $wallets = $this->service->getWallets();

            return $this->response(true, 200, [
                'wallets' => $wallets->wallets ?? []
            ]);
        } catch (\Exception $exception) {
            Handler::log($exception);
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }

    public function lockWallet(Request $request) {
        try {
            RequestValidator::validate($request, new WalletRequest());
            $this->service->lockWallet($request->wallet_id);

            return $this->response(true, 200);
        } catch (\Exception $exception) {
            Handler::log($exception);
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }

    public function unlockWallet(Request $request) {
        try {
            RequestValidator::validate($request, new WalletRequest());
            $this->service->unlockWallet($request->wallet_id);

            return $this->response(true, 200);
        } catch (\Exception $exception) {
            Handler::log($exception);
            return $this->response(false, 419, ['message' => $exception->getMessage()]);
        }
    }
}
