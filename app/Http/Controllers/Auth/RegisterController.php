<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\Handler;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\RequestValidators\RequestValidator;
use App\Services\RegisterService;
use App\Traits\Authenticate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RegisterController extends Controller
{
    use Authenticate;

    protected RegisterService $service;

    public function __construct(RegisterService $service) {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function registerAndLogin(Request $request): Response
    {
        try {
            RequestValidator::validate($request, new RegisterRequest());
            $token = $this->service->register($request);

            return  $this->respondWithToken($token);
        } catch (\Exception $exception) {
            Handler::log($exception);
        }
        return $this->response(false, 422, ['message' => $exception->getMessage()]);
    }
}
