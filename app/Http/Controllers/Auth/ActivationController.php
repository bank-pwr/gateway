<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\Handler;
use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ActivationController extends Controller
{
    public function activate(Request $request, string $activationToken) {
        try {
            $user = User::where(User::ACTIVATION_TOKEN, $activationToken)->first();

            if (!is_null($user)) {
                if ($user->{User::ACTIVE}) {
                    return $this->response(false, 419, ['message' => 'AUTH.ACTIVATION.USER_ALREADY_ACTIVE']);
                }
                $user->{User::ACTIVE} = true;
                $user->{User::EMAIL_VERIFIED_AT} = Carbon::now();
                $user->save();

                return $this->response(true, 200, ['message' => 'AUTH.ACTIVATION.SUCCESS']);
            }
        } catch (\Exception $exception) {
            Handler::log($exception);
        }
        return $this->response(false, 404, ['message' => 'AUTH.ACTIVATION.INVALID_TOKEN']);
    }
}
