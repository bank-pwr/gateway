<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\Handler;
use App\Http\Controllers\Controller;
use App\Http\Requests\CodeRequest;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use App\RequestValidators\RequestValidator;
use App\Services\LoginService;
use App\Traits\Authenticate;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    use Authenticate;

    protected LoginService $service;

    public function __construct(LoginService $service) {
        $this->service = $service;
    }

    public function login(Request $request) {
        RequestValidator::validate($request, new LoginRequest());
        try {
            $this->service->login($request);
            return $this->response(true, 200);
        } catch (\Exception $exception) {
            Handler::log($exception);
        }
        return $this->response(false, 401, ['message' => 'AUTH.LOGIN.INVALID_DATA']);
    }

    public function generateToken(Request $request) {
        RequestValidator::validate($request, new CodeRequest());
        try {
            $user = User::fromEmail($request->email);
            $token = $this->service->generateToken($user, $request->code);
            return $this->respondWithToken($token);
        } catch (\Exception $exception) {
            Handler::log($exception);
        }
        return $this->response(false, 401, ['message' => 'AUTH.LOGIN.INVALID_DATA']);
    }
}
