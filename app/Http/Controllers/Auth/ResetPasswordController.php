<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\Handler;
use App\Http\Controllers\Controller;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\SendPasswordChangeEmailRequest;
use App\RequestValidators\RequestValidator;
use App\Services\PasswordService;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    protected PasswordService $service;

    public function __construct(PasswordService $service)
    {
        $this->service = $service;
    }

    public function sendResetEmail(Request $request) {
        try {
            if (RequestValidator::isValid($request, new SendPasswordChangeEmailRequest())) {
                $email = $request->get('email');
                $this->service->sendResetMail($email);
            }
        } catch (\Exception $exception) {
            Handler::log($exception);
        }
        return $this->response(true, 200, ['message' => 'AUTH.PASSWORD.RESET.SUCCESS']);
    }

    public function resetPassword(Request $request) {
        try {
            RequestValidator::validate($request, new ResetPasswordRequest());
            $token = $request->get('token');
            $newPassword = $request->get('password');

            if (!$this->service->resetPassword($token, $newPassword)) {
                throw new \Exception('Reset password failed');
            }

            return $this->response(true, 200, ['message' => 'AUTH.PASSWORD.RESET.SUCCESS']);
        } catch (\Exception $exception) {
            Handler::log($exception);
        }
        return $this->response(false, 419, ['message' => 'AUTH.PASSWORD.RESET.KEY_INVALID']);
    }
}
