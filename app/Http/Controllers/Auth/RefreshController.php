<?php

namespace App\Http\Controllers\Auth;

use App\Authentication\Facades\Token;
use App\Exceptions\Handler;
use App\Http\Controllers\Controller;
use App\Traits\Authenticate;
use Illuminate\Http\Request;

class RefreshController extends Controller
{
    use Authenticate;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function refresh(Request $request) {
        try {
            $token = $request->bearerToken();
            if (is_null($token)) {
                return $this->response(false, 401, ['message' => 'AUTH.REFRESH.TOKEN_NOT_PROVIDED']);
            }

            $refreshedToken = Token::refresh($token);
            return $this->respondWithToken($refreshedToken);
        } catch (\Exception $exception) {
            Handler::log($exception);
        }
        return $this->response(false, $exception->getCode(), ['message' => $exception->getMessage()]);
    }
}
