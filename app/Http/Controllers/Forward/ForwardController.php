<?php

namespace App\Http\Controllers\Forward;

use App\Http\Controllers\Controller;
use App\Services\ForwardService;
use Illuminate\Http\Request;

class ForwardController extends Controller
{
    protected ForwardService $service;

    public function __construct(ForwardService $service)
    {
        $this->service = $service;
    }

    public function forward(Request $request, string $module, string $route = null) {
        try {
            $response = $this->service->forward($request, $module, $route);
            $headers = $response->getHeaders();

            if (array_key_exists('Transfer-Encoding', $headers)) {
                unset($headers['Transfer-Encoding']);
            }

            return response($response->getBody()->getContents(), $response->getStatusCode(), $headers);
        } catch (\Exception $exception) {
            return $this->response(false, 401, ['message' => 'FORWARD.FAIL']);
        }
    }
}
