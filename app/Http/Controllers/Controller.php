<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function response(bool $success, int $status, array $message = []) {
        $response = [
            'success' => $success,
            'message' => ''];

        if (!empty($message)) {
            $response = array_merge($response, $message);
        }

        return response($response, $status);
    }
}
