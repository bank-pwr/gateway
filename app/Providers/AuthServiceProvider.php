<?php

namespace App\Providers;

use App\Authentication\Facades\Token;
use App\Models\User;
use App\Traits\Authenticate;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    use Authenticate;
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['auth']->viaRequest('firebase-jwt', function (Request $request) {
            $token = $request->bearerToken();

            if (!is_null($token)) {
                try {
                    $user = Token::decodeToken($token);
                    if (!$user) throw new \Exception;
                } catch (\Exception $e) {
                    return null;
                }
                return User::find($user['user']->id);
            }
            return null;
        });

        $this->app['auth']->viaRequest('modules', function (Request $request) {
            $id = $request->route('id');

            return User::find($id);
        });
    }
}
