<?php

namespace App\Providers;

use App\Events\SendPasswordResetMailEvent;
use App\Events\TransactionAcceptedEvent;
use App\Events\UserRegisteredEvent;
use App\Listeners\LogEmailListener;
use App\Listeners\SendPasswordResetMailListener;
use App\Listeners\TransactionAcceptedListener;
use App\Listeners\UserRegisteredListener;
use Illuminate\Mail\Events\MessageSent;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserRegisteredEvent::class => [
            UserRegisteredListener::class,
        ],
        MessageSent::class => [
            LogEmailListener::class,
        ],
        SendPasswordResetMailEvent::class => [
            SendPasswordResetMailListener::class,
        ],
        TransactionAcceptedEvent::class => [
            TransactionAcceptedListener::class,
        ]
    ];
}
