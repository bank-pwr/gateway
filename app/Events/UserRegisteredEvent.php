<?php

namespace App\Events;

use App\Models\User;

class UserRegisteredEvent extends Event
{
    public User $user;
    /**
     * UserRegisteredEvent constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
