<?php

namespace App\Events;

use App\Models\User;

class TransactionAcceptedEvent extends Event
{
    public User $user;
    public float $transactionValue;

    public function __construct(User $user, float $transactionValue)
    {
        $this->user = $user;
        $this->transactionValue = $transactionValue;
    }
}
