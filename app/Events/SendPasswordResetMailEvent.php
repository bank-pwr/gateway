<?php

namespace App\Events;

use App\Models\PasswordReset;

class SendPasswordResetMailEvent extends Event
{
    public PasswordReset $reset;

    /**
     * SendPasswordResetMailEvent constructor.
     * @param PasswordReset $reset
     */
    public function __construct(PasswordReset $reset)
    {
        $this->reset = $reset;
    }
}
