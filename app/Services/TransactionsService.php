<?php

namespace App\Services;

use App\Exceptions\BadResponseException;
use GuzzleHttp\Client;

class TransactionsService extends ApiService
{
    const MODULE = 'transactions';

    public function __construct(Client $client) {
        parent::__construct($client, self::MODULE);
    }

    /**
     * @param int $userId
     * @return mixed|string
     * @throws BadResponseException
     */
    public function createNewWallet(int $userId) {
        return $this->post('create-wallet', [
            'user_id' => $userId
        ]);
    }

    /**
     * @param int $userId
     * @return mixed|string
     * @throws BadResponseException
     */
    public function getUserInfo(int $userId) {
        return $this->get('user-info', [
            'user_id' => $userId
        ]);
    }

    /**
     * @param int $userId
     * @return mixed|string
     * @throws BadResponseException
     */
    public function getUserTransactions(int $userId) {
        return $this->get('user-transactions', [
            'user_id' => $userId
        ]);
    }

    /**
     * @return mixed|string
     * @throws BadResponseException
     */
    public function getWallets() {
        return $this->get('get-wallets');
    }

    /**
     * @param int $walletId
     * @return mixed|string
     * @throws BadResponseException
     */
    public function lockWallet(int $walletId) {
        return $this->post('lock-wallet', [
            'wallet_id' => $walletId
        ]);
    }

    /**
     * @param int $walletId
     * @return mixed|string
     * @throws BadResponseException
     */
    public function unlockWallet(int $walletId) {
        return $this->post('unlock-wallet', [
            'wallet_id' => $walletId
        ]);
    }

    /**
     * @return mixed|string
     * @throws BadResponseException
     */
    public function getAllTransactions() {
        return $this->get('transactions/all');
    }

    /**
     * @param string $fromWalletNumber
     * @param string $toWalletNumber
     * @param float $amount
     * @return mixed|string
     * @throws BadResponseException
     */
    public function makeTransaction(string $fromWalletNumber, string $toWalletNumber, float $amount) {
        return $this->post('transactions/make', [
            'from_wallet_number' => $fromWalletNumber,
            'to_wallet_number' => $toWalletNumber,
            'amount' => $amount,
        ]);
    }

    /**
     * @param int $transactionId
     * @return mixed|string
     * @throws BadResponseException
     */
    public function acceptTransaction(int $transactionId) {
        return $this->post('transactions/accept', [
            'transaction_id' => $transactionId
        ]);
    }

    /**
     * @param int $transactionId
     * @return mixed|string
     * @throws BadResponseException
     */
    public function rollbackTransaction(int $transactionId) {
        return $this->post('transactions/rollback', [
            'transaction_id' => $transactionId
        ]);
    }

    /**
     * @param string $route
     * @param array $params
     * @param array $headers
     * @return mixed|string
     * @throws BadResponseException
     */
    private function get(string $route, array $params = [], array $headers = []) {
        $options = $this->getRequestOptions('GET', $params, $headers);

        $response = $this->client
            ->get($this->getUrl($route), $options)
            ->getBody()
            ->getContents();

        $response = json_decode($response);

        if (!$response || !$response->success) {
            throw new BadResponseException();
        }

        return $response;
    }

    /**
     * @param string $route
     * @param array $params
     * @param array $headers
     * @return mixed|string
     * @throws BadResponseException
     */
    private function post(string $route, array $params = [], array $headers = []) {
        $options = $this->getRequestOptions('POST', $params, $headers);

        $response = $this->client
            ->post($this->getUrl($route), $options)
            ->getBody()
            ->getContents();

        $response = json_decode($response);

        if (!$response || !$response->success) {
            throw new BadResponseException();
        }

        return $response;
    }
}
