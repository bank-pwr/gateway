<?php

namespace App\Services;

use App\Authentication\Facades\Token;
use App\Exceptions\BadGoogleCodeException;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Sonata\GoogleAuthenticator\GoogleAuthenticator;
use Sonata\GoogleAuthenticator\GoogleQrUrl;

class LoginService
{
    protected GoogleAuthenticator $googleAuthenticator;

    public function __construct() {
        $this->googleAuthenticator = new GoogleAuthenticator();
    }

    /**
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function login(Request $request): string {
        $user = $this->getUserFromCredentials($request->toArray());

        if (is_null($user)) {
            throw new \Exception('User not found exception', 401);
        }

        return GoogleQrUrl::generate($user->{User::EMAIL}, decrypt($user->{User::SECRET}));
    }

    /**
     * @param User $user
     * @param string $code
     * @return string
     * @throws BadGoogleCodeException
     */
    public function generateToken(User $user, string $code): string {
        if (!$this->googleAuthenticator->checkCode(decrypt($user->{User::SECRET}), $code)) {
            throw new BadGoogleCodeException();
        }
        $payload = Token::getPayload($user);
        $token = Token::issueToken($payload);
        Token::store($token);

        return $token;
    }


    protected function getUserFromCredentials(array $credentials): ?User {
        $user = User::where(User::EMAIL, $credentials['email'])->first();
        if (! ($user && Hash::check($credentials['password'], $user->password))) {
            return  null;
        }
        return  $user;
    }
}
