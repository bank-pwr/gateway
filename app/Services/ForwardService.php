<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ForwardService
{
    protected Client $client;
    protected string $module;
    protected ?string $route;

    public function __construct(Client $client) {
        $this->client = $client;
    }

    public function forward(Request $request, string $module, ?string $route) {
        $this->module = $module;
        $this->route = $route;

        return $this->client->request(
            $request->method(),
            $this->getUrl(),
            $this->getRequestOptions($request)
        );
    }

    protected function getUrl() {

        return config('modules.'.$this->module.'.url').'/'.$this->route;
    }

    protected function getHeaders(array $headers = []): array {
        $externalKey = config('modules.'.$this->module.'.api_key');
        $basicHeaders = [
            'Module-key' => $externalKey,
        ];

        $headers = array_merge($basicHeaders, $headers);
        return  $headers;
    }

    protected function getRequestOptions(Request $request): array {
        $user = (Auth::user())->toArray();
        $request->headers->remove('Content-Type');
        $request->headers->remove('Authorization');

        if ($request->method() !== 'GET') {
            return [
                'headers' => $this->getHeaders($request->headers->all()),
                'form_params' => $this->getRequestParams($request, $user),
                'http_errors' => false,
            ];
        }
        return [
            'headers' => $this->getHeaders($request->headers->all()),
            'query' => $this->getRequestParams($request, $user),
            'http_errors' => false,
        ];
    }

    protected function getRequestParams(Request $request, ?array $user = null):array {
        $params = $request->all();

        if (!is_null($user)) {
            return array_merge($params, ['user' => $user]);
        }
        return $params;
    }
}
