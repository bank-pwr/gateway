<?php

namespace App\Services;

use App\Events\SendPasswordResetMailEvent;
use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PasswordService
{
    public function sendResetMail(string $email):void {
        $user = $this->findUser($email);

        if (!is_null($user)) {
            $password = $this->generateResetToken($email);
            event(new SendPasswordResetMailEvent($password));
        }
    }

    public function resetPassword(string $resetToken, string $newPassword): bool {
        DB::beginTransaction();
        $passwordReset = PasswordReset::where(PasswordReset::TOKEN, $resetToken)->first();
        $hash = Hash::make($newPassword);
        $user = $this->findUser($passwordReset->{PasswordReset::EMAIL});

        if (is_null($passwordReset) || is_null($user)) {
            return false;
        }

        $user->{User::PASSWORD} = $hash;
        $user->save();
        $passwordReset->delete();
        DB::commit();

        return true;
    }

    protected function generateResetToken(string $email): PasswordReset {
        $passwordReset = PasswordReset::where(PasswordReset::EMAIL, $email)->first();

        if (!is_null($passwordReset)) {
            return $passwordReset;
        }

        return PasswordReset::create([
            PasswordReset::EMAIL => $email,
            PasswordReset::TOKEN => Str::random(40)
        ]);
    }

    protected function findUser(string $email): ?User {
        return User::where(User::EMAIL, $email)->first();
    }
}
