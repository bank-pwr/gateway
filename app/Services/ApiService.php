<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

abstract class ApiService
{
    protected Client $client;
    protected string $module;
    protected ?string $route;

    public function __construct(Client $client, string $module) {
        $this->client = $client;
        $this->module = $module;
    }

    public function forward(Request $request, string $module, ?string $route) {
        $this->module = $module;
        $this->route = $route;

        return $this->client->request(
            $request->method(),
            $this->getUrl(),
            $this->getRequestOptions($request)
        );
    }

    protected function getUrl(string $route = null) {
        if (!is_null($route)) {
            $this->route = $route;
        }
        return config('modules.'.$this->module.'.url').'/'.$this->route;
    }

    protected function getHeaders(array $headers = []): array {
        $externalKey = config('modules.'.$this->module.'.api_key');
        $basicHeaders = [
            'Module-key' => $externalKey,
        ];

        $headers = array_merge($basicHeaders, $headers);
        return  $headers;
    }

    protected function getRequestOptions(string $method, array $params = [], array $headers = []): array {
        $user = (Auth::user());

        if ($user) {
            $user = $user->toArray();
        }

        if ($method !== 'GET') {
            return [
                'headers' => $this->getHeaders($headers),
                'form_params' => $this->getRequestParams($params, $user ?? []),
                'http_errors' => false,
            ];
        }
        return [
            'headers' => $this->getHeaders($headers),
            'query' => $this->getRequestParams($params, $user),
            'http_errors' => false,
        ];
    }

    protected function getRequestParams(array $params = [], ?array $user = null):array {
        if (!is_null($user)) {
            return array_merge($params, ['user' => $user]);
        }
        return $params;
    }
}
