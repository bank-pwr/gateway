<?php

namespace App\Services;

use App\Authentication\Facades\Token;
use App\Events\UserRegisteredEvent;
use App\Exceptions\Handler;
use App\Jobs\SendActivationMailJob;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Sonata\GoogleAuthenticator\GoogleAuthenticator;
use Spatie\Permission\Models\Role;

class RegisterService
{
    protected TransactionsService $transactionsService;

    public function __construct(TransactionsService $transactionsService) {
        $this->transactionsService = $transactionsService;
    }

    /**
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function register(Request $request): string {
        DB::beginTransaction();
        $user = $this->create($request->toArray());
        event(new UserRegisteredEvent($user));

        $payload = Token::getPayload($user);
        $token = Token::issueToken($payload);
        Token::store($token);
        DB::commit();

        return $token;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     * @throws \Exception
     */
    protected function create(array $data)
    {
        try {
            DB::beginTransaction();

            $user = User::create([
                User::PHONE => $data['phone'],
                User::SECRET => encrypt((new GoogleAuthenticator())->generateSecret()),
                User::EMAIL => $data['email'],
                User::PASSWORD => Hash::make($data['password']),
                User::ACTIVATION_TOKEN => Str::random(80),
                User::ACTIVE => 0,
            ]);

            $this->transactionsService->createNewWallet($user->{User::ID});
            $user->assignRole(Role::findByName(User::ROLE_CUSTOMER));
            DB::commit();

            return $user;
        } catch (\Exception $exception) {
            DB::rollBack();
            Handler::log($exception);
            throw $exception;
        }
    }
}
