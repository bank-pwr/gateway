<?php

namespace App\Authentication\Facades;

use App\Models\User;
use Illuminate\Support\Facades\Facade;

/**
 * @method static void store(string $token)
 * @method static bool exists(string $token)
 * @method static void delete(string $token)
 * @method static void  putOnBlackList(string $token)
 * @method static bool isBlacklisted(string $token)
 * @method static int getExpirationTime()
 * @method static string issueToken(array $payload)
 * @method static array|null decodeToken(string $token)
 * @method static array getPayload(User $user)
 * @method static string refresh(string $token)
 */
class Token extends Facade
{
    protected static function getFacadeAccessor() {
        return static::class;
    }
}
