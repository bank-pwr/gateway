<?php

namespace App\Authentication\Stores;

use App\Authentication\Contracts\TokenStore;
use Illuminate\Support\Facades\Cache as CacheFacade;

class Cache implements TokenStore
{
    protected int $tokenTime;

    public function __construct() {
        $this->tokenTime = config('auth.tokens.expiration_time', 3600);
    }

    public function store(string $token): void {
        CacheFacade::put($this->getStorageKey($token), true, $this->tokenTime);
    }

    public function exists(string $token): bool {
        $tokenInCache = CacheFacade::get($this->getStorageKey($token));

        return !is_null($tokenInCache);
    }

    public function delete(string $token): void {
        CacheFacade::pull($this->getStorageKey($token));
    }

    public function putOnBlackList(string $token): void {
        CacheFacade::forever($this->getBlacklistKey($token), true);
    }

    public function isBlacklisted(string $token): bool {
        $tokenInBlacklist = CacheFacade::get($this->getBlacklistKey($token));

        return !is_null($tokenInBlacklist);
    }

    protected function getStorageKey(string $token): string {
        return 'jwt.keys.'.$token;
    }

    protected function getBlacklistKey(string $token): string {
        return 'jwt.blacklist.'.$token;
    }
}
