<?php

namespace App\Authentication\Contracts;

interface TokenStore
{
    public function store(string $token): void ;

    public function exists(string $token): bool ;

    public function delete(string $token): void ;

    public function putOnBlackList(string $token): void ;

    public function isBlacklisted(string $token): bool ;

}
