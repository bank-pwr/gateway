<?php

namespace App\Authentication\Helpers;

use App\Authentication\Contracts\TokenStore;
use App\Exceptions\AuthKeyNotFoundException;
use App\Exceptions\InvalidTokenException;
use App\Exceptions\TokenBlacklistedException;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Support\Carbon;

class TokenHelper
{
    protected TokenStore $store;
    private string $privateKey;
    private string $publicKey;

    /**
     * TokenHelper constructor.
     * @param TokenStore $store
     * @throws AuthKeyNotFoundException
     */
    public function __construct(TokenStore $store)
    {
        $this->store = $store;

        $this->privateKey = config('auth.keys.private', null);
        $this->publicKey = config('auth.keys.public', null);

        if (is_null($this->privateKey) || is_null($this->publicKey)) {
            throw new AuthKeyNotFoundException();
        }
    }

    /**
     * @param string $token
     */
    public function store(string $token): void {
        $this->store->store($token);
    }

    /**
     * @param string $token
     * @return bool
     */
    public function exists(string $token): bool {
        return $this->store->exists($token);
    }

    /**
     * @param string $token
     */
    public function delete(string $token): void {
        $this->store->delete($token);
    }

    /**
     * @param string $token
     * @return bool
     */
    public function isBlacklisted(string $token): bool {
        return $this->store->isBlacklisted($token);
    }

    /**
     * @param string $token
     */
    public function putOnBlackList(string $token): void {
        $this->store->putOnBlackList($token);
    }

    /**
     * @return int
     */
    public function getExpirationTime(): int {
        return config('auth.tokens.expiration_time', 3600);
    }

    /**
     * @param array $payload
     * @return string
     */
    public function issueToken(array $payload): string {
        return JWT::encode($payload, $this->privateKey, 'RS256');
    }

    /**
     * @param string $token
     * @return array
     * @throws InvalidTokenException
     */
    public function decodeToken(string $token): array {
        try {
            return (array) JWT::decode($token, $this->publicKey, ['RS256']);
        } catch (\Exception $exception) {
            throw new InvalidTokenException();
        }
    }

    public function getPayload(User $user): array {
        return  [
            'user' => $user->toArray(),
            'expires_at' => Carbon::now()->addSeconds($this->getExpirationTime())->getTimestamp()
        ];
    }

    protected function regeneratePayload(array $payload): array {
        $payload['expires_at'] = Carbon::now()->addSeconds($this->getExpirationTime())->getTimestamp();
        return $payload;
    }

    /**
     * @param string $token
     * @return string
     * @throws InvalidTokenException
     * @throws TokenBlacklistedException
     */
    public function refresh(string $token): string {
        if ($this->isBlacklisted($token)) {
            throw new TokenBlacklistedException();
        }
        $payload = $this->regeneratePayload($this->decodeToken($token));

        $this->putOnBlackList($token);
        $this->delete($token);

        $regeneratedToken = $this->issueToken($payload);
        $this->store($regeneratedToken);

        return $regeneratedToken;
    }
}
