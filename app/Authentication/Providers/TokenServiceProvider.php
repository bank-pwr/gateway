<?php

namespace App\Authentication\Providers;

use App\Authentication\Contracts\TokenStore;
use App\Authentication\Facades\Token;
use App\Authentication\Helpers\TokenHelper;
use App\Authentication\Stores\Cache;
use Illuminate\Support\ServiceProvider;

class TokenServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * Stores
         */
        $this->app->bind(TokenStore::class, function () {
            $driver = config('auth.tokens.store_driver');

            if (is_null($driver)) {
                return $this->app->make(Cache::class);
            }
            return $this->app->make('App\Authentication\Stores\\'.$driver);
        });


        /**
         * Binding facade
         */
        $this->app->bind(Token::class, function() {
            return $this->app->make(TokenHelper::class);
        });

    }

    public function boot() {

    }
}
