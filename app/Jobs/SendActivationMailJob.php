<?php

namespace App\Jobs;

use App\Mail\TransactionAcceptedMail;
use App\Mail\UserRegisteredMail;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class SendActivationMailJob extends Job
{
    protected User $user;

    /**
     * SendActivationMailJob constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->user->{User::EMAIL})->send(new UserRegisteredMail($this->user));
    }
}
