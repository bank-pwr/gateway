<?php

namespace App\Jobs;

use App\Mail\TransactionAcceptedMail;
use App\Mail\UserRegisteredMail;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class SendTransactionInfoMailJob extends Job
{
    protected User $user;
    protected float $transactionValue;

    /**
     * SendTransactionInfoMailJob constructor.
     * @param User $user
     * @param float $transactionValue
     */
    public function __construct(User $user, float $transactionValue)
    {
        $this->user = $user;
        $this->transactionValue = $transactionValue;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->user->{User::EMAIL})->send(new TransactionAcceptedMail($this->user, $this->transactionValue));
    }
}
