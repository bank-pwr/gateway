<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable as Authenticate;
use Illuminate\Contracts\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Lumen\Auth\Authorizable as AuthorizableTrait;
use Spatie\Permission\Traits\HasRoles;

class User extends Model implements Authenticatable, Authorizable
{
    use SoftDeletes, Authenticate, AuthorizableTrait, HasRoles;

    const TABLE = 'users';
    const ID = 'id';
    const PHONE = 'phone';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const EMAIL_VERIFIED_AT = 'email_verified_at';
    const ACTIVATION_TOKEN = 'activation_token';
    const ACTIVE = 'active';
    const SECRET = 'secret';

    const ROLE_ADMIN = 'ADMIN';
    const ROLE_CALL_CENTER = 'CALL_CENTER';
    const ROLE_CUSTOMER = 'CUSTOMER';

    protected $table = self::TABLE;
    protected $primaryKey = self::ID;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::EMAIL, self::PASSWORD, self::ACTIVE, self::PHONE,
        self::ACTIVATION_TOKEN, self::EMAIL_VERIFIED_AT,
        self::SECRET
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::PASSWORD, self::ACTIVATION_TOKEN
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::EMAIL_VERIFIED_AT => 'datetime',
    ];

    protected $dates = [
        self::EMAIL_VERIFIED_AT, self::CREATED_AT, self::UPDATED_AT
    ];

    public static function fromEmail(string $email):?self {
        return self::where(self::EMAIL, $email)->first();
    }

    public function getQrCode(): string {
        return decrypt($this->{self::SECRET});
    }
}
