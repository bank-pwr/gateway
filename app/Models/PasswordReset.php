<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PasswordReset extends Model
{
    use SoftDeletes;

    const TABLE = 'password_resets';
    const ID = 'id';
    const EMAIL = 'email';
    const TOKEN = 'token';

    protected $table = self::TABLE;
    protected $primaryKey = self::ID;

    protected $fillable = [
        self::EMAIL, self::TOKEN
    ];
}
