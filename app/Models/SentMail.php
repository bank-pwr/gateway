<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SentMail extends Model
{
    const TABLE = 'sent_mails';
    const ID = 'id';
    const RECEIVER = 'receiver';
    const SUBJECT = 'subject';
    const MESSAGE = 'message';

    protected $table = self::TABLE;
    protected $primaryKey = self::ID;

    protected $fillable = [
        self::RECEIVER, self::SUBJECT, self::MESSAGE
    ];
}
