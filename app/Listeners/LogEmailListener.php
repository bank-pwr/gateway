<?php

namespace App\Listeners;

use App\Models\SentMail;
use Illuminate\Mail\Events\MessageSent;

class LogEmailListener
{
    protected SentMail $sentMail;

    /**
     * LogEmailListener constructor.
     * @param SentMail $mail
     */
    public function __construct(SentMail $mail)
    {
        $this->sentMail = $mail;
    }

    /**
     * Handle the event.
     *
     * @param  MessageSent  $event
     * @return void
     */
    public function handle(MessageSent $event)
    {
        $this->sentMail->create([
            SentMail::RECEIVER => array_key_first($event->message->getTo()),
            SentMail::SUBJECT => $event->message->getSubject(),
            SentMail::MESSAGE => $event->message->getBody(),
        ]);
    }
}
