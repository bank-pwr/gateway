<?php

namespace App\Listeners;

use App\Events\SendPasswordResetMailEvent;
use App\Events\UserRegisteredEvent;
use App\Mail\PasswordResetMail;
use App\Mail\UserRegisteredMail;
use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class SendPasswordResetMailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param SendPasswordResetMailEvent $event
     */
    public function handle(SendPasswordResetMailEvent $event)
    {
        Mail::to($event->reset->{PasswordReset::EMAIL})
            ->send(new PasswordResetMail($event->reset->{PasswordReset::TOKEN}));
    }
}
