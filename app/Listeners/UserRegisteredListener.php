<?php

namespace App\Listeners;

use App\Events\UserRegisteredEvent;
use App\Jobs\SendActivationMailJob;

class UserRegisteredListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param UserRegisteredEvent $event
     */
    public function handle(UserRegisteredEvent $event)
    {
        dispatch(new SendActivationMailJob($event->user));
    }
}
