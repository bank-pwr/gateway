<?php

namespace App\Listeners;

use App\Events\TransactionAcceptedEvent;
use App\Jobs\SendTransactionInfoMailJob;

class TransactionAcceptedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param TransactionAcceptedEvent $event
     */
    public function handle(TransactionAcceptedEvent $event)
    {
        dispatch(new SendTransactionInfoMailJob($event->user, $event->transactionValue));
    }
}
