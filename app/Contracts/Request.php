<?php

namespace App\Contracts;

interface Request
{
    public function rules(): array;
    public function messages(): array;
}
