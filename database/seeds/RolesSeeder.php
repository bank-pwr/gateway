<?php

namespace Database\Seeds;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleAdmin = Role::create(['name' => 'ADMIN']);
        $roleCallCenter = Role::create(['name' => 'CALL_CENTER']);
        $roleCustomer = Role::create(['name' => 'CUSTOMER']);

        $permissionBlock = Permission::create(['name' => 'BLOCK_ACCOUNT']);
        $permissionAcceptTransaction = Permission::create(['name' => 'ACCEPT_TRANSACTION']);
        $permissionMakeTransaction = Permission::create(['name' => 'MAKE_TRANSACTION']);

        $roleAdmin->givePermissionTo($permissionBlock);
        $roleAdmin->givePermissionTo($permissionAcceptTransaction);

        $roleCallCenter->givePermissionTo($permissionAcceptTransaction);

        $roleCustomer->givePermissionTo($permissionMakeTransaction);

    }
}
