<?php

use Illuminate\Database\Migrations\Migration;

class PrepareRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        (new Database\Seeds\RolesSeeder())->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }
}
