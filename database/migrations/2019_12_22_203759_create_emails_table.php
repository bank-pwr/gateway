<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailsTable extends Migration
{
    const TABLE = 'sent_mails';
    const ID = 'id';
    const RECEIVER = 'receiver';
    const SUBJECT = 'subject';
    const MESSAGE = 'message';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::TABLE)) {
            Schema::create(self::TABLE, function (Blueprint $table) {
                $table->bigIncrements(self::ID);
                $table->string(self::RECEIVER);
                $table->string(self::SUBJECT);
                $table->text(self::MESSAGE);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::TABLE);
    }
}
