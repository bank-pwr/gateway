<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    const TABLE = 'users';
    const ID = 'id';
    const PHONE = 'phone';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const EMAIL_VERIFIED_AT = 'email_verified_at';
    const ACTIVATION_TOKEN = 'activation_token';
    const ACTIVE = 'active';
    const SECRET = 'secret';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::TABLE)) {
            Schema::create(self::TABLE, function (Blueprint $table) {
                $table->bigIncrements(self::ID);
                $table->string(self::EMAIL)->unique();
                $table->timestamp(self::EMAIL_VERIFIED_AT)->nullable();
                $table->string(self::PASSWORD);
                $table->text(self::SECRET);
                $table->string(self::PHONE);
                $table->boolean(self::ACTIVE)->default(false);
                $table->string(self::ACTIVATION_TOKEN);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(self::TABLE);
        Schema::enableForeignKeyConstraints();
    }
}
