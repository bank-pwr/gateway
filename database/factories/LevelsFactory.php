<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\Levels::class, function (Faker\Generator $faker) {
    $level = \App\Models\Levels::orderBy(\App\Models\Levels::LEVEL, 'desc')->first();
    if (is_null($level)) {
        $level = 1;
    } else {
        $level = $level->{\App\Models\Levels::LEVEL} ++;
    }
    return [
        \App\Models\Levels::LEVEL => $level,
        \App\Models\Levels::THRESHOLD => 40
    ];
});
