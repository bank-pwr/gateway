# Aplikacja bankowa - API Gateway

Projekt aplikacji bankowej składa się z kilku elementów:

- Aplikacji klienckiej
- Aplikacji backendowej
- Mikroserwisu odpowiedzialnego za transakcje bankowe

## Założenia

Głównym założeniem projektu było opracowanie systemu usługowego z naciskiem na wydzielenie mikroserwisu, którego zadaniem jest przetwarzanie operacji bankowych.

Warto również zauważyć, że w aplikacji zaimplementowano autoryzację dwuetapową z użyciem Google Authenticator.

## Funkcjonalności

Aplikacja przede wszystkim oferuje:

- Logowanie i rejestrację z użyciem JWT
- Uwierzytelnianie dwuetapowe
- Role i dwa typy kont: klienta oraz pracownika banku
- Otwieranie kont bankowych
- Wykonywanie przelewów między kontami wraz z pełną historią operacji bankowych
- Blokowanie kont przez pracowników banku
- Akceptację i odrzucanie operacji bankowych przez pracowników banku
