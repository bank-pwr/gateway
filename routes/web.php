<?php

$router->group(['prefix' => 'auth', 'middleware' => ['json']], function () use ($router) {
    $router->post('register', [
        'as' => 'auth.register',
        'uses' => 'Auth\RegisterController@registerAndLogin',
    ]);
    $router->post('login', [
        'as' => 'auth.login',
        'uses' => 'Auth\LoginController@login',
    ]);
    $router->post('generate-token', [
        'as' => 'auth.token',
        'uses' => 'Auth\LoginController@generateToken',
    ]);
    $router->post('user', [
        'as' => 'auth.user',
        'uses' => 'Users\UserController@user',
        'middleware' => ['auth']
    ]);
    $router->post('refresh', [
        'as' => 'auth.refresh',
        'uses' => 'Auth\RefreshController@refresh',
    ]);
    $router->post('activate/{activationToken}', [
        'as' => 'auth.activate',
        'uses' => 'Auth\ActivationController@activate'
    ]);
    $router->post('password/send-reset', [
        'as' => 'auth.password.send-reset',
        'uses' => 'Auth\ResetPasswordController@sendResetEmail'
    ]);

    $router->post('password/reset', [
        'as' => 'auth.password.reset',
        'uses' => 'Auth\ResetPasswordController@resetPassword'
    ]);
});

$router->group(['prefix' => 'transactions', 'middleware' => ['json']], function () use ($router) {
    $router->group(['middleware' => 'permission:MAKE_TRANSACTION'], function () use ($router) {
        $router->post('make', 'Users\TransactionsController@makeTransaction');
        $router->get('history', 'Users\TransactionsController@getUserTransactions');
    });

    $router->group(['middleware' => 'permission:ACCEPT_TRANSACTION'], function () use ($router) {
        $router->get('all', 'Users\TransactionsController@getAllTransactions');
        $router->post('accept', 'Users\TransactionsController@acceptTransaction');
        $router->post('rollback', 'Users\TransactionsController@rollbackTransaction');
    });

    $router->group(['middleware' => 'permission:BLOCK_ACCOUNT'], function () use ($router) {
        $router->post('lock-wallet', 'Users\TransactionsController@lockWallet');
        $router->post('unlock-wallet', 'Users\TransactionsController@unlockWallet');
        $router->get('get-wallets', 'Users\TransactionsController@getWallets');
    });
});

$router->get('test', function() {
    return 'testt';
});
