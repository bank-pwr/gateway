<?php

namespace Tests\Feature\Auth;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Tests\Feature\Auth\Traits\AuthRequests;
use Tests\Feature\Auth\Traits\PrepareUserBeforeLogin;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use DatabaseMigrations, PrepareUserBeforeLogin, AuthRequests;


    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    public function testLogin() {
        $this->prepareUserBeforeLogin();
        $response = $this->loginRequest($this->getUserData());

        $response->assertResponseStatus(200);
    }

    public function testLoginResponse() {
        $this->prepareUserBeforeLogin();
        $response = $this->loginRequest($this->getUserData());
        $response = json_decode($response->response->content(), true);

        $this->assertIsArray($response);
        $this->assertArrayHasKey('access_token', $response);
    }
}
