<?php

namespace Tests\Feature\Auth;

use App\Mail\PasswordResetMail;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Mail;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Tests\Feature\Auth\Traits\AuthRequests;
use Tests\Feature\Auth\Traits\PrepareUserBeforeLogin;
use Tests\TestCase;

class PasswordResetTest extends TestCase
{
    use DatabaseMigrations, PrepareUserBeforeLogin, AuthRequests;

    public function testMailSend() {
        Mail::fake();
        $this->prepareUserBeforeLogin();

        $user = $this->getUserData();
        $response = $this->resetMailRequest($user['email']);

        Mail::assertSent(PasswordResetMail::class);
        $response->assertResponseStatus(200);
    }

    public function testMailNotRegistered() {
        Mail::fake();
        $response = $this->resetMailRequest('test@test.test');

        Mail::assertNothingSent();
        $response->assertResponseStatus(200);
    }

    public function testBadMail() {
        Mail::fake();
        $response = $this->resetMailRequest('definitely_not_mail');


        Mail::assertNothingSent();
        $response->assertResponseStatus(200);
    }

    public function testMultipleMailsForOneEmail() {
        Mail::fake();
        $this->prepareUserBeforeLogin();

        $user = $this->getUserData();
        for ($i = 0; $i < 2; $i++) {
            $response = $this->resetMailRequest($user['email']);
            $response->assertResponseStatus(200);
        }
        Mail::assertSent(PasswordResetMail::class, 2);
    }

    public function testPasswordReset() {
        Mail::fake();
        $this->prepareUserBeforeLogin();
        $user = $this->getUserData();
        $this->resetMailRequest($user['email']);

        $passwordReset = PasswordReset::where(PasswordReset::EMAIL, $user['email'])->first();

        $response = $this->resetPasswordRequest($passwordReset->{PasswordReset::TOKEN}, 'test1234');

        $response->assertResponseStatus(200);
    }

    public function testMultipleResetsWithSameToken() {
        Mail::fake();
        $this->prepareUserBeforeLogin();
        $user = $this->getUserData();
        $this->resetMailRequest($user['email']);

        $passwordReset = PasswordReset::where(PasswordReset::EMAIL, $user['email'])->first();

        $this->resetPasswordRequest($passwordReset->{PasswordReset::TOKEN}, 'test1234');
        $response = $this->resetPasswordRequest($passwordReset->{PasswordReset::TOKEN}, 'test1234');

        $response->assertResponseStatus(419);
    }

    public function testLoginWithNewPassword() {
        Mail::fake();
        $this->prepareUserBeforeLogin();
        $user = $this->getUserData();
        $this->resetMailRequest($user['email']);
        $passwordReset = PasswordReset::where(PasswordReset::EMAIL, $user['email'])->first();

        $this->resetPasswordRequest($passwordReset->{PasswordReset::TOKEN}, 'test1234');
        $credentials = [
            'email' => $user['email'],
            'password' => 'test1234'
        ];
        $response = $this->loginRequest($credentials);


        $response->assertResponseStatus(200);
        $response = json_decode($response->response->content(), true);

        $this->assertIsArray($response);
        $this->assertArrayHasKey('access_token', $response);
    }
}
