<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Tests\Feature\Auth\Traits\AuthRequests;
use Tests\Feature\Auth\Traits\PrepareUserBeforeLogin;
use Tests\TestCase;

class ActivationTest extends TestCase
{
    use DatabaseMigrations, PrepareUserBeforeLogin, AuthRequests;

    public function testUserActivate() {
        $this->prepareUserBeforeLogin();

        $user = $this->getUser();
        $token = $user->{User::ACTIVATION_TOKEN};

        $response = $this->activateRequest($token);

        $response->assertResponseStatus(200);
        $user = $this->getUser();

        $this->assertNotNull($user->{User::EMAIL_VERIFIED_AT});
        $this->assertEquals(1, $user->{User::ACTIVE});
    }

    public function testActiveTokenNotFound() {
        $token = 'aaaaa';
        $response = $this->activateRequest($token);

        $response->assertResponseStatus(404);
    }

    private function getUser(): User {
        $userData = $this->getUserData();
        return User::where(User::EMAIL, $userData['email'])->first();
    }

}
