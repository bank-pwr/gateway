<?php

namespace Tests\Feature\Auth\Traits;

use Tests\TestCase;

trait AuthRequests
{

    public function activateRequest(string $token): TestCase {
        return $response = $this->post('auth/activate/'.$token);
    }

    public function loginRequest(array $payload): TestCase {
        return $this->post('auth/login', $payload);
    }

    public function registerRequest(array $payload): TestCase {
        return $this->post('auth/register', $payload);
    }

    public function refreshRequest(string $token): TestCase {
        return $this->post('auth/refresh', [], [
            'Authorization' => "Bearer {$token}"
        ]);
    }

    public function resetMailRequest(string $email): TestCase {
        return $this->post('auth/password/send-reset', [
            'email' => $email,
        ]);
    }

    public function resetPasswordRequest(string $token, string $password): TestCase {
        return $this->post('auth/password/reset', [
            'token' => $token,
            'password' => $password,
            'password_confirmation' => $password
        ]);
    }
}
