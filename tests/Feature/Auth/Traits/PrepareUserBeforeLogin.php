<?php

namespace Tests\Feature\Auth\Traits;

use Tests\TestCase;

trait PrepareUserBeforeLogin
{
    use UserData;

    public function prepareUserBeforeLogin(): TestCase {
        return $this->post('auth/register', $this->getUserData());
    }
}
