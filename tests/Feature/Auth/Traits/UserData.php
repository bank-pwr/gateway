<?php

namespace Tests\Feature\Auth\Traits;

trait UserData
{
    public function getUserData(): array {
        return [
            'name' => 'test',
            'email' => 'test@email.com',
            'password' => '12345678',
            'password_confirmation' => '12345678'

        ];
    }
}
