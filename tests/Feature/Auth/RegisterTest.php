<?php

namespace Tests\Feature\Auth;

use App\Mail\UserRegisteredMail;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Mail;
use Tests\Feature\Auth\Traits\AuthRequests;
use Tests\Feature\Auth\Traits\UserData;
use Tests\TestCase;

/**
 * Class RegisterTest
 */
class RegisterTest extends TestCase
{
    use DatabaseMigrations, UserData, AuthRequests;

    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    public function testRegister() {
        Mail::fake();

        $response = $this->registerRequest($this->getUserData());

        Mail::assertSent(UserRegisteredMail::class);
        $response->assertResponseStatus(200);
    }

    public function testRegisterResponse() {
        $response = $this->registerRequest($this->getUserData());
        $response = json_decode($response->response->content(), true);

        $this->assertIsArray($response);
        $this->assertArrayHasKey('access_token', $response);
    }
}
