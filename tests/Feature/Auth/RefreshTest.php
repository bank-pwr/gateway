<?php

namespace Tests\Feature\Auth;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Tests\Feature\Auth\Traits\AuthRequests;
use Tests\Feature\Auth\Traits\PrepareUserBeforeLogin;
use Tests\TestCase;

class RefreshTest extends TestCase
{
    use DatabaseMigrations, PrepareUserBeforeLogin, AuthRequests;

    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    public function testRefresh() {
        $token = $this->getAccessToken();
        $response = $this->refreshRequest($token);

        $response->assertResponseStatus(200);
    }

    public function testRefreshResponse() {
        $token = $this->getAccessToken();

        $response = $this->refreshRequest($token);
        $response = json_decode($response->response->content(), true);

        $this->assertIsArray($response);
        $this->assertArrayHasKey('access_token', $response);
    }

    private function getAccessToken(): string {
        $response = $this->prepareUserBeforeLogin();

        $response = json_decode($response->response->content(), true);

        return $response['access_token'];
    }
}
