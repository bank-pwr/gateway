<?php

namespace Tests\Unit;

use App\Authentication\Facades\Token;
use Illuminate\Support\Carbon;
use App\Models\User;
use Tests\TestCase;

class AuthorizationTokenTest extends TestCase
{
    private array $payload;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $user = (new User)->toArray();
        $time = Carbon::now()->getTimestamp();

        $this->payload = [
            'user' => $user,
            'expires_at' => $time
        ];
    }

    public function testIssueToken() {
        $token = Token::issueToken($this->payload);

        $this->assertIsString($token);
    }

    public function testStoreToken() {
        $token = Token::issueToken($this->payload);
        Token::store($token);

        $this->assertTrue(Token::exists($token));
    }

    public function testDeleteToken() {
        $token = Token::issueToken($this->payload);
        Token::store($token);
        Token::delete($token);

        $this->assertFalse(Token::exists($token));
    }

    public function testDecodeToken() {
        $token = Token::issueToken($this->payload);

        $payload = Token::decodeToken($token);

        $this->assertEquals($this->payload, $payload);
    }

    public function testIsBlacklisted() {
        $token = Token::issueToken($this->payload);

        $this->assertFalse(Token::isBlacklisted($token));
    }

    public function testPutOnBlacklist() {
        $token = Token::issueToken($this->payload);
        Token::putOnBlackList($token);

        $this->assertTrue(Token::isBlacklisted($token));
    }

    public function testGetPayload() {
        $payload = Token::getPayload(new User);

        $this->assertIsArray($payload);
    }

    public function testRefresh() {
        $token = Token::issueToken($this->payload);
        $refreshedToken = Token::refresh($token);

        $this->assertIsString($refreshedToken);
    }

    public function testBlacklistAfterRefresh() {
        $token = Token::issueToken($this->payload);
        Token::refresh($token);

        $this->assertTrue(Token::isBlacklisted($token));
    }

    public function testExistsRefreshedToken() {
        $token = Token::issueToken($this->payload);
        $refreshedToken = Token::refresh($token);

        $this->assertTrue(Token::exists($refreshedToken));
    }
}
