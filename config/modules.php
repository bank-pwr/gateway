<?php

return [
    'transactions' => [
        'ip' => env('TRANSACTIONS_IP', null),
        'url' => env('TRANSACTIONS_URL', null),
        'gateway_key' => env('TRANSACTIONS_GATEWAY_KEY', null),
        'api_key' => env('TRANSACTIONS_API_KEY', null),
    ],
];
